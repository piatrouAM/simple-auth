from django.contrib import admin

from .models import Domain, Urls


@admin.register(Domain)
class DomainAdmin(admin.ModelAdmin):
    list_display = ('name', 'method', 'active', 'allow_default')
    list_filter = ('active', 'method')
    search_fields = ('name', 'description')
    list_editable = ['active']


@admin.register(Urls)
class UrlsAdmin(admin.ModelAdmin):
    list_display = ('id', 'priority', 'name', 'path', 'site',
                    'authorizationRequired', 'active')
    list_filter = ('active', 'site')
    search_fields = ('name', 'site', 'notes')
    list_editable = ['active']
