from django.http.response import HttpResponse
from django.http.request import HttpRequest

from permissions.models import Domain

from lib.classes.HttpResponseEmpty import HtppResponseEmpty
from lib.functions.notActiveSite import notActiveSite
from lib.functions.searchPath import searchPath


def HttpMethod(req: HttpRequest) -> HttpResponse:
    """Check user authorization and permissions

    Args:
        req (HttpRequest): Django HttpRequest

    Returns:
        HttpResponse: Django HttpResponse
    """
    host = (req.META['HTTP_X_ORIGINAL_HOST']
            if 'HTTP_X_ORIGINAL_HOST' in req.META else req.headers['Host'])
    path = (req.META['HTTP_X_ORIGINAL_URI']
            if 'HTTP_X_ORIGINAL_URI' in req.META else req.path)
    site = Domain.objects.filter(name=host).first()
    if not site:
        return HtppResponseEmpty(status=403)
    if not site.active:
        return notActiveSite(req)
    url = searchPath(site, path)
    if url is None:
        return HtppResponseEmpty(req.user,
                                 status=(200 if site.allow_default else 403))
    if not url.authorizationRequired:
        return HtppResponseEmpty(req.user)
    if req.user.is_authenticated and url.authorizationRequired:
        return HtppResponseEmpty(req.user)
    return HtppResponseEmpty(status=403)
