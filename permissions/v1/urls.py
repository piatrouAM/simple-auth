from django.urls import path

from permissions.v1.http import HttpMethod

urlpatterns = [
    path('http/', HttpMethod, name='Api v1 http method'),
]
