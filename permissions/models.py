import re

from django.db import models

AUTH_METHODS = [
    ('HTTP', 'HTTP'),
]


class Domain(models.Model):
    """Target site model
    """
    name = models.CharField(max_length=253)
    description = models.TextField()
    active = models.BooleanField(default=True)
    method = models.CharField(max_length=16,
                              choices=AUTH_METHODS,
                              default='HTTP')
    allow_default = models.BooleanField(default=True)


class Urls(models.Model):
    """Pattern to check user permissions
    """
    name = models.CharField(max_length=253)
    active = models.BooleanField(default=True)
    site = models.ForeignKey(
        Domain,
        on_delete=models.CASCADE,
    )
    notes = models.TextField()
    authorizationRequired = models.BooleanField()
    path = models.TextField()
    priority = models.IntegerField()

    def checkPath(self, path: str) -> bool:
        """Checks path by pattern

        Args:
            path (str): Request path

        Returns:
            bool: Check result
        """
        return re.match(self.path, path)

    class Meta:
        ordering = ['-priority']
