from typing import Any
import json

from django.http.response import HttpResponse
from django.contrib.auth.models import User


class HtppResponseEmpty(HttpResponse):
    """Empty Response object
    """

    def __init__(self, user: User = None, *args: Any, **kwargs: Any) -> None:
        """Create empty response object with user data

        Args:
            user (User, optional): User model. Defaults to None.
            status (int, optional): Status code. Defaults to 200
        """
        super().__init__(content='', *args, **kwargs)
        if self.status_code >= 400:
            return
        if user is not None and user.is_authenticated:
            self.headers['x-user'] = json.dumps({
                'id': user.id,
                'username': user.username,
                'first_name': user.first_name,
                'last_name': user.last_name,
                'email': user.email,
                'authorized': True
            })
        else:
            self.headers['x-user'] = '{}'
