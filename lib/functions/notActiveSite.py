from django.http.response import HttpResponse
from django.http.request import HttpRequest

from lib.classes.HttpResponseEmpty import HtppResponseEmpty


def notActiveSite(req: HttpRequest) -> HttpResponse:
    """Return result if site not active

    Args:
        req (HttpRequest): Django HttpRequest

    Returns:
        HttpResponse: Django HttpResponse
    """
    return HtppResponseEmpty(req.user)
