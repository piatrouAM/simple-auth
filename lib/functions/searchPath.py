from permissions.models import Domain, Urls


def searchPath(site: Domain, path: str) -> Urls:
    """Seeks a priority path through patterns

    Args:
        site (Domain): Target site
        path (str): Request path

    Returns:
        Urls: Priority pattern
    """
    siteUrls = Urls.objects.filter(site=site)
    for url in siteUrls:
        if url.checkPath(path):
            return url
    return None
